# MindTheWord

An extension for Google Chrome that helps people learn new languages while they browse the web.

MindTheWord helps the user to easily learn the vocabulary of a new language
while browsing pages in his native language. In every web page visited, it
randomly translates a few words into the language he would like to learn.
Since only a few words are translated, it is easy to infer their meaning from
the context.

## Usage:

1. `npm install -g cordova`
2. Read the platform guidelines and install the [requirements](https://cordova.apache.org/docs/en/latest/guide/platforms/android/)
2. `cordova build android` - To build Android APK
3. `cordova run browser` - To test in default browser

How to Contribute
-------------
Coming Soon


Licenses
--------

* GNU-GPL-3.0

* CC-By-NC-ND [![License](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-nd/4.0/)


